# @html-validate/puppeteer-fetch changelog

## [2.0.0](https://gitlab.com/html-validate/puppeteer-fetch/compare/v1.0.0...v2.0.0) (2023-05-07)

### ⚠ BREAKING CHANGES

- drop support for NodeJS 14
- require node 14

### Features

- drop support for NodeJS 14 ([8e3eb3e](https://gitlab.com/html-validate/puppeteer-fetch/commit/8e3eb3e550f497a382633f972351260fbdb01aa4))
- require node 14 ([5149676](https://gitlab.com/html-validate/puppeteer-fetch/commit/5149676e280bf9a27b879539550a116c266c05a6))

### Dependency upgrades

- **deps:** update dependency puppeteer to v13.7.0 ([ec62714](https://gitlab.com/html-validate/puppeteer-fetch/commit/ec62714876f12d630dfd9093c6aa9c3dbc88a6cf))
- **deps:** update dependency puppeteer to v14 ([4cb0251](https://gitlab.com/html-validate/puppeteer-fetch/commit/4cb0251ffd955cd67a731fe80125d6706974be74))
- **deps:** update dependency puppeteer to v14.1.1 ([f53cd21](https://gitlab.com/html-validate/puppeteer-fetch/commit/f53cd21da8cd6661d9efaee16f0c21c8d45218b3))
- **deps:** update dependency puppeteer to v14.1.2 ([66a0b5e](https://gitlab.com/html-validate/puppeteer-fetch/commit/66a0b5eba1dc0c9d4bc54a2e9ee935e89a0d1ba3))
- **deps:** update dependency puppeteer to v14.2.1 ([8db5877](https://gitlab.com/html-validate/puppeteer-fetch/commit/8db5877f4cdb8ff8279df197a2b6451f60bb8746))
- **deps:** update dependency puppeteer to v14.3.0 ([9f6267d](https://gitlab.com/html-validate/puppeteer-fetch/commit/9f6267d9362421b50874fb2e7cd83ea0af0b68d7))
- **deps:** update dependency puppeteer to v14.4.0 ([3be2571](https://gitlab.com/html-validate/puppeteer-fetch/commit/3be2571396bee6b2840d4e3df0d7c6c3f851c978))
- **deps:** update dependency puppeteer to v14.4.1 ([7d1e5b1](https://gitlab.com/html-validate/puppeteer-fetch/commit/7d1e5b1a95d7cec177c4109e54cb3d8031936a00))
- **deps:** update dependency puppeteer to v15 ([03808d5](https://gitlab.com/html-validate/puppeteer-fetch/commit/03808d57b6f9a3ea6d2646c3c3f353c4cbc865e7))
- **deps:** update dependency puppeteer to v15.3.0 ([471eb58](https://gitlab.com/html-validate/puppeteer-fetch/commit/471eb58aa299041ecc7e3e7051224f3a0a5dae65))
- **deps:** update dependency puppeteer to v15.3.1 ([54eb895](https://gitlab.com/html-validate/puppeteer-fetch/commit/54eb8955d0164306a06e9ca599cded88e7c7512d))
- **deps:** update dependency puppeteer to v15.3.2 ([74031c8](https://gitlab.com/html-validate/puppeteer-fetch/commit/74031c82ef31834f73ef482d3ee050ce5e4a96f0))
- **deps:** update dependency puppeteer to v15.4.0 ([7136add](https://gitlab.com/html-validate/puppeteer-fetch/commit/7136add76dc52b559bfaae1782be87abeaa078e3))
- **deps:** update dependency puppeteer to v15.5.0 ([c93e287](https://gitlab.com/html-validate/puppeteer-fetch/commit/c93e2877d6ad4ee3020e051d3dda419d9ed84138))
- **deps:** update dependency puppeteer to v16 ([0b1d847](https://gitlab.com/html-validate/puppeteer-fetch/commit/0b1d84719fb46d7d11b4cdd22f9ed585b8dd1e24))
- **deps:** update dependency puppeteer to v16.1.0 ([94e72f3](https://gitlab.com/html-validate/puppeteer-fetch/commit/94e72f3be7de2e233b54df600e784dd1fec0e04e))
- **deps:** update dependency puppeteer to v16.1.1 ([e27bd93](https://gitlab.com/html-validate/puppeteer-fetch/commit/e27bd93dd2b2f1ca61f3fb023f2d8733391332fa))
- **deps:** update dependency puppeteer to v16.2.0 ([b0e366e](https://gitlab.com/html-validate/puppeteer-fetch/commit/b0e366e09c095560a7161bf389c8638c711c3467))
- **deps:** update dependency puppeteer to v17 ([bea65e1](https://gitlab.com/html-validate/puppeteer-fetch/commit/bea65e1857e1d3e5edc7b9047a616f87c89745a8))
- **deps:** update dependency puppeteer to v18 ([613d707](https://gitlab.com/html-validate/puppeteer-fetch/commit/613d707fb40328a7432c37feef9a5461e88323b4))
- **deps:** update dependency puppeteer to v19 ([d4eb23f](https://gitlab.com/html-validate/puppeteer-fetch/commit/d4eb23f4ae07e2f800de26aae3955ef5e40aaee2))
- **deps:** update dependency puppeteer to v19.1.0 ([ac59642](https://gitlab.com/html-validate/puppeteer-fetch/commit/ac59642f0105d8bdc203fed4e8e632c52247088c))
- **deps:** update dependency puppeteer to v19.10.0 ([3aa0ded](https://gitlab.com/html-validate/puppeteer-fetch/commit/3aa0ded8a89b91608e063880b91988a564aee249))
- **deps:** update dependency puppeteer to v19.10.1 ([bf53781](https://gitlab.com/html-validate/puppeteer-fetch/commit/bf537814f1c6a8cc5a76b09c008f4461ce6e2c55))
- **deps:** update dependency puppeteer to v19.11.0 ([2f3c24f](https://gitlab.com/html-validate/puppeteer-fetch/commit/2f3c24f4d1303f735694545c7bfe871207fffc28))
- **deps:** update dependency puppeteer to v19.11.1 ([fbd08ca](https://gitlab.com/html-validate/puppeteer-fetch/commit/fbd08cad1355364c08d8344eb394cc6fed0ee470))
- **deps:** update dependency puppeteer to v19.2.0 ([844eee0](https://gitlab.com/html-validate/puppeteer-fetch/commit/844eee053791cb31c24ea164bf1981d4f441db3d))
- **deps:** update dependency puppeteer to v19.2.2 ([9694643](https://gitlab.com/html-validate/puppeteer-fetch/commit/96946434b337ae1fa7f5586ebdccf690cc872172))
- **deps:** update dependency puppeteer to v19.3.0 ([0374b3c](https://gitlab.com/html-validate/puppeteer-fetch/commit/0374b3c4b49e18aa61c35e0b97c911e2e4805501))
- **deps:** update dependency puppeteer to v19.4.0 ([eb78bea](https://gitlab.com/html-validate/puppeteer-fetch/commit/eb78beaba249b1283839604d138581e5da21ea36))
- **deps:** update dependency puppeteer to v19.4.1 ([87439ed](https://gitlab.com/html-validate/puppeteer-fetch/commit/87439ed9863725dbc3478ee8def1fc68e167a395))
- **deps:** update dependency puppeteer to v19.5.0 ([bc74fc7](https://gitlab.com/html-validate/puppeteer-fetch/commit/bc74fc7550b94b3e0522282a66c15ade77d409a2))
- **deps:** update dependency puppeteer to v19.5.1 ([1cd9c29](https://gitlab.com/html-validate/puppeteer-fetch/commit/1cd9c29bf6c29b92a696213bb19ed29d7b98439b))
- **deps:** update dependency puppeteer to v19.5.2 ([69eb79e](https://gitlab.com/html-validate/puppeteer-fetch/commit/69eb79e97a6f1b416b08589101d458381b5318f0))
- **deps:** update dependency puppeteer to v19.6.0 ([eff7d38](https://gitlab.com/html-validate/puppeteer-fetch/commit/eff7d38c3e5a4f09f24ba7f59a3b3be26743f9c3))
- **deps:** update dependency puppeteer to v19.6.1 ([dcef49c](https://gitlab.com/html-validate/puppeteer-fetch/commit/dcef49ccbeca5ff6383619ce692d8144f65703b8))
- **deps:** update dependency puppeteer to v19.6.2 ([4b96c24](https://gitlab.com/html-validate/puppeteer-fetch/commit/4b96c248a192d8564c3643cbd04d8a7977be3b58))
- **deps:** update dependency puppeteer to v19.6.3 ([fc5d519](https://gitlab.com/html-validate/puppeteer-fetch/commit/fc5d519da43ecd3e9c88add8671fdd21071b941e))
- **deps:** update dependency puppeteer to v19.7.0 ([cbe0208](https://gitlab.com/html-validate/puppeteer-fetch/commit/cbe0208fa3f8f14e5ac443f9c49c1eaa2fa62e0a))
- **deps:** update dependency puppeteer to v19.7.1 ([a8eb238](https://gitlab.com/html-validate/puppeteer-fetch/commit/a8eb238bb72761fef4479f7faf5256a5548e229d))
- **deps:** update dependency puppeteer to v19.7.2 ([203fe1d](https://gitlab.com/html-validate/puppeteer-fetch/commit/203fe1d4ada4df9b10a0642699e256f152fda781))
- **deps:** update dependency puppeteer to v19.7.4 ([7630f12](https://gitlab.com/html-validate/puppeteer-fetch/commit/7630f12a1aaf6f3e660c8a324dd4e371ad539dd6))
- **deps:** update dependency puppeteer to v19.7.5 ([63f19fa](https://gitlab.com/html-validate/puppeteer-fetch/commit/63f19fa1c5d7c4002dde0b7c3183cb798fd0d680))
- **deps:** update dependency puppeteer to v19.8.0 ([ef1920f](https://gitlab.com/html-validate/puppeteer-fetch/commit/ef1920f0c5a5ef2b2022369e6c72cdb3f34617f6))
- **deps:** update dependency puppeteer to v19.8.1 ([83681e8](https://gitlab.com/html-validate/puppeteer-fetch/commit/83681e8376616fd76b8a5e5390998f4a1fb26e36))
- **deps:** update dependency puppeteer to v19.8.2 ([f8570f3](https://gitlab.com/html-validate/puppeteer-fetch/commit/f8570f386c385164733883697059c1d4bd863800))
- **deps:** update dependency puppeteer to v19.8.3 ([3d19912](https://gitlab.com/html-validate/puppeteer-fetch/commit/3d1991299246613db6abdc59e7ad45e43980b744))
- **deps:** update dependency puppeteer to v19.8.4 ([3f22c6e](https://gitlab.com/html-validate/puppeteer-fetch/commit/3f22c6e227c85c01d0fb1a5630a51f54efef3259))
- **deps:** update dependency puppeteer to v19.8.5 ([e7cdd3b](https://gitlab.com/html-validate/puppeteer-fetch/commit/e7cdd3b55c8a181ff9b573dd92b22e969409cbdb))
- **deps:** update dependency puppeteer to v19.9.0 ([eabcea5](https://gitlab.com/html-validate/puppeteer-fetch/commit/eabcea5ec742b5727c99252751314539deac18f9))
- **deps:** update dependency puppeteer to v19.9.1 ([305c524](https://gitlab.com/html-validate/puppeteer-fetch/commit/305c5240b0e52771560dff37897b0e42950ca228))
- **deps:** update dependency puppeteer to v20 ([63b44a8](https://gitlab.com/html-validate/puppeteer-fetch/commit/63b44a8d71d7bebeace197471254215f76d70cb7))

## 1.0.0 (2022-04-25)

### Features

- initial version ([9e545bb](https://gitlab.com/html-validate/puppeteer-fetch/commit/9e545bbf5cc362c9fd083f3730d3fa35aa9d55e0))

### Dependency upgrades

- **deps:** update dependency puppeteer to v13.6.0 ([44abb8d](https://gitlab.com/html-validate/puppeteer-fetch/commit/44abb8d46b0e9058c4d142ab5005f6b4ef4f4867))
