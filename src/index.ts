import { promises as fs } from "fs";
import path from "path";
import puppeteer from "puppeteer";
import { ArgumentParser } from "argparse";

interface PackageJson {
	name: string;
	description: string;
	version: string;
}

interface Args {
	url: string;
}

function normalizeUrl(url: string): string {
	if (/:\/\//.exec(url)) {
		return url;
	} else {
		return `https://${url}`;
	}
}

async function run(): Promise<void> {
	const content = await fs.readFile(path.join(__dirname, "../package.json"), "utf-8");
	const pkg = JSON.parse(content) as PackageJson;
	const parser = new ArgumentParser({
		description: pkg.description,
	});

	parser.add_argument("-v", "--version", { action: "version", version: pkg.version });
	parser.add_argument("-f", "--foo", { help: "foo bar" });
	parser.add_argument("-b", "--bar", { help: "bar foo" });
	parser.add_argument("url", { help: "URL to fetch", metavar: "URL" });

	const args = parser.parse_args() as Args;

	const url = normalizeUrl(args.url);
	const browser = await puppeteer.launch({ headless: false });
	const page = await browser.newPage();
	await page.goto(url);
	const source = await page.content();
	await browser.close();

	/* eslint-disable-next-line no-console -- should write result to console */
	console.log(source);
}

run().catch((err: unknown) => {
	/* eslint-disable-next-line no-console -- expected to log */
	console.error(err);
	process.exitCode = 1;
});
