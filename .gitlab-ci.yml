image: node:latest

include:
  - template: Jobs/Dependency-Scanning.gitlab-ci.yml
  - template: Jobs/Secret-Detection.gitlab-ci.yml

stages:
  - prepare
  - build
  - test
  - compatibility
  - release
  - postrelease

secret_detection:
  needs: ["Build"]
  dependencies: ["Build"]
  variables:
    SECRET_DETECTION_EXCLUDED_PATHS: "node_modules"

NPM:
  stage: prepare
  artifacts:
    name: ${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}-npm
    paths:
      - node_modules/
    reports:
      dependency_scanning: gl-dependency-scanning.json
  script:
    - node --version
    - npm --version
    - npm ci --no-fund --no-audit --no-update-notifier
    - npm audit --json --production | npx --yes gitlab-npm-audit-parser -o gl-dependency-scanning.json || true
    - npx sort-package-json --check

Build:
  stage: build
  artifacts:
    name: ${CI_PROJECT_PATH_SLUG}-${CI_PIPELINE_ID}-build
    paths:
      - dist/
      - src/generated
  script:
    - npm run --if-present build
    - npm pack
    - npm exec npm-pkg-lint

ESLint:
  stage: test
  needs: ["NPM", "Build"]
  script:
    - npm exec eslint-config -- --check
    - npm run eslint -- --max-warnings 0

Prettier:
  stage: test
  dependencies: ["NPM"]
  script:
    - npm run prettier:check

.release:
  stage: release
  variables:
    GIT_AUTHOR_NAME: ${GITLAB_USER_NAME}
    GIT_AUTHOR_EMAIL: ${GITLAB_USER_EMAIL}
    GIT_COMMITTER_NAME: ${HTML_VALIDATE_BOT_NAME}
    GIT_COMMITTER_EMAIL: ${HTML_VALIDATE_BOT_EMAIL}
  before_script:
    - npm install -g $(node -p 'require("./package.json").release.extends')

Dry run:
  extends: .release
  needs: []
  dependencies: []
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
    - if: '$CI_COMMIT_REF_NAME =~ /^release\//'
  script:
    - npm exec semantic-release -- --dry-run

Release:
  extends: .release
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master" && $CI_PIPELINE_SOURCE == "web"'
      when: manual
    - if: '$CI_COMMIT_REF_NAME =~ /^release\// && $CI_PIPELINE_SOURCE == "web"'
      when: manual
  script:
    - npm exec semantic-release
